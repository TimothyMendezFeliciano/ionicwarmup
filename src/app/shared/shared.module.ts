import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ProfileComponent, LoginComponent],
  exports: [ProfileComponent, LoginComponent]
})
export class SharedModule { }
