// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  	apiKey: "AIzaSyCXJEGn9ExGnwXSByG0rnBsV-t-8x3gEc8",
    authDomain: "firemastercourse.firebaseapp.com",
    databaseURL: "https://firemastercourse.firebaseio.com",
    projectId: "firemastercourse",
    storageBucket: "firemastercourse.appspot.com",
    messagingSenderId: "1020074756353",
    appId: "1:1020074756353:web:32cd8e83373bd581"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
